#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFrame>
#include <QPushButton>
#include <QSpacerItem>
#include <QPlainTextEdit>
#include <QFileDialog>
#include <QToolBar>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFrame *mainFrame = new QFrame();
    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *buttonsLayout = new QHBoxLayout();

    this->setCentralWidget(mainFrame);

    QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/icons/document-open.png"));
    QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/icons/document-save.png"));
    QIcon saveAsIcon = QIcon::fromTheme("document-save-as", QIcon(":/icons/document-save-as.png"));
    QIcon closeIcon = QIcon::fromTheme("document-close", QIcon(":/icons/window-close.png"));
    QIcon exitIcon = QIcon::fromTheme("application-exit", QIcon(":/icons/application-exit.png"));

    QMenu *fileMenu = ui->menubar->addMenu("File");
    QAction *fileOpenAction = fileMenu->addAction(openIcon, "Open file");
    fileOpenAction->setShortcuts(QKeySequence::Open);
    fileOpenAction->setStatusTip("Open a file from disk");
    QAction *fileSaveAction = fileMenu->addAction(saveIcon, "Save file");
    fileSaveAction->setShortcuts(QKeySequence::Save);
    fileSaveAction->setStatusTip("Save a file to disk");
    QAction *fileSaveAsAction = fileMenu->addAction(saveAsIcon, "Save file as");
    fileSaveAsAction->setShortcuts(QKeySequence::SaveAs);
    fileSaveAsAction->setStatusTip("Save as a file to disk");
    QAction *fileCloseAction = fileMenu->addAction(closeIcon, "Close file");
    fileCloseAction->setShortcuts(QKeySequence::Close);
    fileCloseAction->setStatusTip("Close a file");
    fileMenu->addSeparator();
    QAction *exitAction = fileMenu->addAction(exitIcon, "Exit");
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip("Quit application");

    QAction *aboutAction = ui->menubar->addAction("About");

    QToolBar *topToolBar = new QToolBar();
    topToolBar->addAction(fileOpenAction);
    topToolBar->addAction(fileSaveAction);
    topToolBar->addAction(fileSaveAsAction);
    topToolBar->addAction(fileCloseAction);
    this->addToolBar(topToolBar);

    this->setWindowTitle("Image Processor");
}

MainWindow::~MainWindow()
{
    delete ui;
}

